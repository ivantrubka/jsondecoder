//
//  ViewController.swift
//  TestTask
//
//  Created by Ivan Trubka on 29.01.2018.
//  Copyright © 2018 Ivan Trubka. All rights reserved.
//

import UIKit
import Alamofire


class FirstVC: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    
    var userArray : [UserDataExсeptFriends]?
    let sequeToUserDetails = "sequeToUserDetails"
    
    private var selectedUser: UserDataExсeptFriends?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Alamofire.request("http://www.mocky.io/v2/59c92a123f0000780183f72d").responseJSON { response in
          //  debugPrint(response)
            
            self.handler(response, { (users, error) in
                if error != nil {
                    print("Alarmmmmm !")
                }
                
                self.userArray = users
                self.tableView.reloadData()
            })
        }
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    //MARK: - JSON
    func handler(_ response: DataResponse<Any>, _ completion:@escaping (_ users: [UserDataExсeptFriends]?, _ error: Error?) -> ()) {
        if let error = response.result.error {
            completion(nil, error)
            return
        }
        
        guard let data = response.data else {
            debugPrint("error trying to convert data to JSON")
            return
        }
        
        do {
            let decoder = JSONDecoder()
            let result = try decoder.decode([UserDataExсeptFriends].self, from: data)
            completion(result, nil)
        } catch {
            // handle error
            debugPrint("error trying to convert data to JSON")
            debugPrint(error)
            completion(nil, error)
        }
    }
    
    override func prepare(for seque: UIStoryboardSegue, sender: Any?){
        if seque.identifier == sequeToUserDetails{
            if let vc = seque.destination as? SecondVC{
                vc.userDetails = [selectedUser]
            }
        }
    }
    
}


extension FirstVC: UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = userArray?.count {
            return count
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if let array = userArray{
            selectedUser = array[indexPath.row]
            performSegue(withIdentifier: sequeToUserDetails, sender: self)
        }
    }
    
}

extension FirstVC: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let user = userArray![indexPath.row]
        cell.textLabel?.text = user.name
        cell.detailTextLabel?.text = user.email
    }
    
    
}



















