//
//  SecondVC.swift
//  TestTask
//
//  Created by Ivan Trubka on 31.01.2018.
//  Copyright © 2018 Ivan Trubka. All rights reserved.
//

import UIKit

class SecondVC: UIViewController {
    
    @IBOutlet var userDetailsTableView: UITableView!
    
    public var userDetails: [UserDataExсeptFriends?]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
    }
    
}
extension SecondVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let user = userDetails?[indexPath.row]
        cell.textLabel?.text = user?.address
        
    }

}

extension SecondVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = userDetails?.count{
            return count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return tableView.dequeueReusableCell(withIdentifier: "userCell", for: indexPath)
    }
}
