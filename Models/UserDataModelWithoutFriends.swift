//
//  User.swift
//  TestTask
//
//  Created by Ivan Trubka on 29.01.2018.
//  Copyright © 2018 Ivan Trubka. All rights reserved.
//

import Foundation

struct UserDataExсeptFriends: Codable {
    let about: String
    let address: String
    let age: Int
    let balance: String
    let company: String
    let email: String
    let eyeColor: String
    let favoriteFruit: String
    let gender: String
    let id: String
    let isActive: Bool
    let name: String
    let phone: String
    let picture: URL
    let registered: String
    let tags: Array<String>
}












